package main

import (
	"testing"
)

func TestIsPathLocal(t *testing.T) {

	testCases := []struct {
		Path   string
		Result bool
	}{
		{"file.yaml", true},
		{"patches/file.yaml", true},
		{"patches/", true},
		{"../base", false},
		{"../base/file.yaml", false},
		{"https://localhost/file.yaml", false},
		{"github.com/some/user", false},
		{"github.com/some/user?rev=tag", false},
	}

	for _, testCase := range testCases {
		result := isPathLocal(testCase.Path, "testdata")
		if result != testCase.Result {
			t.Errorf("%s expected %t, got %t", testCase.Path, testCase.Result, result)
		}
	}
}

func TestKustomatization(t *testing.T) {

	config := KupConfig{
		Root:    "testdata",
		CRDs:    "crds",
		Patches: "patches",
		Ignore:  []string{"sensitive"},
	}

	kustomization := PopulateKustomizationFrom(BaseKustomization("testdata"), config)

	found := false
	for _, res := range kustomization.Resources {
		if res == "file.yaml" {
			found = true
			break
		}
	}

	if found == false {
		t.Errorf("expected %s in resources", "file.yaml")
	}

	if len(kustomization.Patches) != 1 {
		t.Errorf("expected 1 patch, got %d", len(kustomization.Patches))
	}

	if kustomization.Patches[0].Path != "patches/file.yaml" {
		t.Errorf("expected patch path %s, got %s", "patches/file.yaml", kustomization.Patches[0].Path)
	}

}
