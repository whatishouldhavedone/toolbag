package main

import (
	"errors"
	"flag"
	"io"
	"io/fs"
	"net/url"
	"os"
	"path"
	"path/filepath"
	"sort"
	"strings"

	"gopkg.in/yaml.v3"
	ktypes "sigs.k8s.io/kustomize/api/types"
)

type KupConfig struct {
	Root    string   `json:"root" yaml:"root"`
	CRDs    string   `json:"crds,omitempty" yaml:"crds,omitempty"`
	Patches string   `json:"patches,omitempty" yaml:"patches,omityempty"`
	Ignore  []string `json:"ignore,omitempty" yaml:"ignore,omitempty"`
}

func getFlagSet(name string) *flag.FlagSet {
	cli := flag.NewFlagSet(name, flag.ExitOnError)

	// Quasi-args.
	cli.String("output", "-", "path to output file (stdout is used by default).")
	// Params
	cli.String("crds", "crds", "relative name of directory holding CRDs.")
	cli.String("patches", "patches", "relative name of directory holding patches.")
	cli.String("ignore", "generators", "comma-separated list of directories to ignore.")
	return cli
}

func NewKustomization() ktypes.Kustomization {
	return ktypes.Kustomization{
		TypeMeta:          ktypes.TypeMeta{Kind: ktypes.KustomizationKind, APIVersion: ktypes.KustomizationVersion},
		OpenAPI:           make(map[string]string),
		CommonLabels:      make(map[string]string),
		CommonAnnotations: make(map[string]string),
	}
}

// Get a list of filenames kustomize recognises.
// This is a copy github.com/sigs.k8s.io/kustomize/api/konfig#RecognizedKustomizationFilenames.
// See https://github.com/kubernetes-sigs/kustomize/blob/7ee6dd551dc141deeb11a2efcc90e36df4f24ab5/api/konfig/general.go
func kustomizeFilenames() []string {
	return []string{
		"kustomization.yaml",
		"kustomization.yml",
		"Kustomization",
	}
}

// Get a base kustomization object to populate.
// If a kustomization file is found in the supplied `dirPath`,
// use it to populate content.
// Otherwise, return a vanilla instance.
func BaseKustomization(dirPath string) ktypes.Kustomization {
	kustomization := NewKustomization()
	for _, filename := range kustomizeFilenames() {
		file, err := os.Open(path.Join(dirPath, filename))
		if err != nil {
			if errors.Is(err, fs.ErrNotExist) {
				continue
			}
			panic(err)
		}
		decoder := yaml.NewDecoder(file)
		if err := decoder.Decode(&kustomization); err != nil {
			if !errors.Is(err, io.EOF) {
				panic(err)
			}
		}
		break
	}
	return kustomization
}

func getWalkFunc(resourceMap map[string][]string, config KupConfig) filepath.WalkFunc {

	return func(path string, info fs.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if info.IsDir() {
			for _, ignored := range config.Ignore {
				if ignored == info.Name() {
					return filepath.SkipDir
				}
			}
		} else {

			for _, kustName := range kustomizeFilenames() {
				if info.Name() == kustName {
					return nil
				}
			}
			relPath, err := filepath.Rel(config.Root, path)
			if err != nil {
				panic(err)
			}
			switch {
			case strings.HasPrefix(path, filepath.Join(config.Root, config.CRDs)):
				resourceMap["crds"] = append(resourceMap["crds"], relPath)
			case strings.HasPrefix(path, filepath.Join(config.Root, config.Patches)):
				resourceMap["patches"] = append(resourceMap["patches"], relPath)
			default:
				resourceMap["resources"] = append(resourceMap["resources"], relPath)
			}
		}
		return nil
	}
}

// Takes a list of paths (i.e. `resources`, `crds`) and returns any that are outside KupConfig.Root.
func filterPaths(resources []string, config KupConfig) []string {
	filteredPaths := []string{}

	for _, resource := range resources {
		if !isPathLocal(resource, config.Root) {
			filteredPaths = append(filteredPaths, resource)
		}
	}
	return filteredPaths
}

func isPathLocal(path, root string) bool {

	if strings.HasPrefix(path, "..") {
		return false
	}

	u, err := url.Parse(path)
	if err == nil && (u.Scheme == "http" || u.Scheme == "https") {
		return false
	}

	absRoot, err := filepath.Abs(root)
	if err != nil {
		panic(err)
	}

	rootDir := os.DirFS(absRoot)

	if _, err := rootDir.(fs.StatFS).Stat(path); errors.Is(err, fs.ErrNotExist) {
		return false
	}
	return true
}

func mergePatches(kustomization ktypes.Kustomization, localPaths []string, root string) []ktypes.Patch {
	patches := make([]ktypes.Patch, 0)

	localPatches := make(map[string]ktypes.Patch)

	for _, patch := range kustomization.Patches {
		if patch.Path == "" {
			patches = append(patches, patch)
		} else {
			if !isPathLocal(patch.Path, root) {
				patches = append(patches, patch)
			} else {
				localPatches[patch.Path] = patch
			}
		}
	}

	for _, path := range localPaths {
		patch, ok := localPatches[path]
		if !ok {
			patch = ktypes.Patch{Path: path}
		}
		patches = append(patches, patch)
	}
	return patches
}

func PopulateKustomizationFrom(base ktypes.Kustomization, config KupConfig) ktypes.Kustomization {
	// Walk directory to find manifests, CRDs, and patches.
	paths := make(map[string][]string)
	err := filepath.Walk(config.Root, getWalkFunc(paths, config))

	basePtr := &base

	if err != nil {
		panic(err)
	}

	// Next check the base kustomization for resources and CRDs.
	crds := append(paths["crds"], filterPaths(base.Crds, config)...)
	resources := append(paths["resources"], filterPaths(base.Resources, config)...)
	sort.Strings(crds)
	sort.Strings(resources)

	sort.Strings(paths["patches"])
	patches := mergePatches(base, paths["patches"], config.Root)

	basePtr.Crds = crds
	basePtr.Resources = resources
	basePtr.Patches = patches

	return base
}

func WriteKustomization(kustomization ktypes.Kustomization, path string) (err error) {
	var writer io.Writer

	if path == "" || path == "-" {
		writer = os.Stdout
	} else {
		if writer, err = os.Create(path); err != nil {
			return err
		}
	}

	encoder := yaml.NewEncoder(writer)
	encoder.SetIndent(2)
	if err := encoder.Encode(kustomization); err != nil {
		return err
	}
	return nil
}

func main() {

	cli := getFlagSet("kup")

	if err := cli.Parse(os.Args[1:]); err != nil {
		panic(err)
	}

	config := KupConfig{
		Root:    cli.Arg(0),
		CRDs:    cli.Lookup("crds").Value.String(),
		Patches: cli.Lookup("patches").Value.String(),
		Ignore:  strings.Split(cli.Lookup("ignore").Value.String(), ","),
	}

	kustomization := PopulateKustomizationFrom(BaseKustomization(config.Root), config)
	outputPath := cli.Lookup("output").Value.String()

	if err := WriteKustomization(kustomization, outputPath); err != nil {
		panic(err)
	}
}
