package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io"
	"io/fs"
	"net/http"
	"os"
	"path"
	"strings"
	"text/template"

	"github.com/Masterminds/sprig/v3"
	"gopkg.in/yaml.v3"
)

type KubernetesMetadata struct {
	Name        string            `json:"name" yaml:"name"`
	Namespace   string            `json:"namespace,omitempty" yaml:"namespace,omitempty"`
	Labels      map[string]string `json:"labels,omitempty" yaml:"labels,omitempty"`
	Annotations map[string]string `json:"annotations,omitempty" yaml:"annotations,omitempty"`
}

type KubernetesObject struct {
	ApiVersion      string                   `json:"apiVersion" yaml:"apiVersion"`
	Kind            string                   `json:"kind" yaml:"kind"`
	Metadata        KubernetesMetadata       `json:"metadata" yaml:"metadata"`
	Spec            map[string]interface{}   `json:"spec,omitempty" yaml:"spec,omitempty"`
	Subjects        []map[string]interface{} `json:"subjects,omitempty" yaml:"subjects,omitempty"`
	Rules           []map[string]interface{} `json:"rules,omitempty" yaml:"rules,omitempty"`
	RoleRef         map[string]interface{}   `json:"roleRef,omitempty" yaml:"roleRef,omitempty"`
	AggregationRule map[string]interface{}   `json:"aggregationRule,omitempty" yaml:"aggregationRule,omitempty"`
	Data            map[string]string        `json:"data,omitempty" yaml:"data,omitempty"`
	Type            string                   `json:"type,omitempty" yaml:"type,omitempty"`
	BinaryData      map[string]string        `json:"binaryData,omitempty" yaml:"binaryData,omitempty"`
	StringData      map[string]string        `json:"stringData,omitempty" yaml:"stringData,omitempty"`
	Items           []KubernetesObject       `json:"items,omitempty" yaml:"items,omitempty"`
}

// A generic encoder type, to simplify dealing with yaml/json/other encodings for manifests.
type Encoder interface {
	Encode(interface{}) error
}

// A generic encoder type, to simplify decoding json/yaml(/other?).
type Decoder interface {
	Decode(any) error
}

func SaveObject(object KubernetesObject, writer io.Writer) error {
	return SaveObjectAs(object, writer, "yaml")
}

func SaveObjectAs(object KubernetesObject, writer io.Writer, format string) error {
	var encoder Encoder

	switch format {
	case "json":
		encoder = json.NewEncoder(writer)
	case "yaml":
		encoder = yaml.NewEncoder(writer)
		encoder.(*yaml.Encoder).SetIndent(2)
		defer encoder.(*yaml.Encoder).Close()
	default:
		return fmt.Errorf("unsupported format, supported formats are: json, yaml")
	}
	return encoder.Encode(object)
}

func ExtractManifests(reader io.Reader, filenameTemplate *template.Template, root string) error {
	return ExtractManifestsFrom(reader, filenameTemplate, root, "yaml")
}

func ExtractManifestsFrom(reader io.Reader, filenameTemplate *template.Template, root, format string) error {
	var decoder Decoder

	switch format {
	case "json":
		decoder = json.NewDecoder(reader)
	case "yaml":
		decoder = yaml.NewDecoder(reader)
	default:
		return fmt.Errorf("unsupported format, supported formats are: json, yaml")
	}
	for {
		object := new(KubernetesObject)
		err := decoder.Decode(object)

		//TODO: Need to decide whether to bail when encountering an issue, or to proceed after printing a warning.
		if err != nil {
			if errors.Is(err, io.EOF) {
				return nil
			}
			return err
		}
		/* Under some conditions, there can be empty manifests, often something like:
		 * ```
		 * ---
		 * # Source: some/path.yaml
		 * ---
		 * apiVersion: v1
		 *  ```
		 * This check avoids creating files for them, and for any seriously malformed manifest.
		 */
		if object.Kind == "" && object.ApiVersion == "" {
			continue
		}

		if object.Kind == "List" {
			for _, item := range object.Items {
				if err := PersistObject(item, filenameTemplate, root, format); err != nil {
					return err
				}
			}
		} else {

			if err := PersistObject(*object, filenameTemplate, root, format); err != nil {
				return err
			}
		}

	}

}

func PersistObject(object KubernetesObject, filenameTemplate *template.Template, root, format string) error {
	var filenameBuffer bytes.Buffer
	if err := filenameTemplate.Execute(&filenameBuffer, object); err != nil {
		return err
	}
	objectPath := path.Join(root, filenameBuffer.String())
	if err := os.MkdirAll(path.Dir(objectPath), 01750); err != nil && !errors.Is(err, fs.ErrExist) {
		return err
	}

	objectFile, err := os.Create(objectPath)
	if err != nil {
		return err
	}
	defer objectFile.Close()
	if err = SaveObject(object, objectFile); err != nil {
		return err
	}

	return err
}

// Return an io.Reader from a file or URL.
func ReaderFor(location string) (reader io.Reader, err error) {

	if strings.HasPrefix(location, "http://") || strings.HasPrefix(location, "https://") {
		resp, err := http.Get(location)
		if err != nil {
			return reader, err
		}
		if resp.StatusCode != 200 {
			err = fmt.Errorf("unexpected status code: %d", resp.StatusCode)
		}

		return resp.Body, err
	}
	return os.Open(location)
}

func ProcessFile(location, root string, filenameTemplate *template.Template) error {
	var reader io.Reader
	reader, err := ReaderFor(location)
	if err != nil {
		return err
	}

	if err := ExtractManifests(reader, filenameTemplate, root); err != nil {
		return err
	}
	return nil
}

func getFilenameTemplate(tpl string) *template.Template {
	// Using sprig here may be overkill for most needs, but does bring
	// standardisation to template handling across tools in this repo.
	return template.Must(
		template.New("filename").Funcs(sprig.FuncMap()).Parse(tpl),
	)
}

func getFlagSet(name string) *flag.FlagSet {
	cli := flag.NewFlagSet(name, flag.ExitOnError)

	workingDirectory, err := os.Getwd()
	if err != nil {
		panic(err)
	}
	cli.String("root", workingDirectory, "directory where manifests will be written.")
	cli.String("template", `{{.Kind|lower}}-{{.Metadata.Name|lower}}.yaml`, "template for file names.")
	return cli
}

func main() {
	cli := getFlagSet("kut")
	if err := cli.Parse(os.Args[1:]); err != nil {
		panic(err)
	}

	root := cli.Lookup("root").Value.String()
	filenameTemplate := getFilenameTemplate(cli.Lookup("template").Value.String())
	files := cli.Args()

	if len(files) < 1 {
		files = append(files, "/dev/stdin")
	}

	for _, inputFile := range files {
		if err := ProcessFile(inputFile, root, filenameTemplate); err != nil {
			panic(err)
		}
	}
}
