package main

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"path"
	"testing"

	"gopkg.in/yaml.v3"
)

func TestProcessFile(t *testing.T) {

	cli := getFlagSet("test")
	cli.Parse([]string{})
	nameTemplate := getFilenameTemplate(cli.Lookup("template").Value.String())

	testCases := []struct {
		Name    string
		Input   string
		Creates []string
	}{
		{Name: "single", Input: "testdata/single.yaml", Creates: []string{"namespace-test.yaml"}},
		{Name: "multiple", Input: "testdata/multiple.yaml", Creates: []string{"namespace-test.yaml", "service-test.yaml", "configmap-test.yaml", "deployment-test.yaml"}},
	}

	for _, testCase := range testCases {
		t.Run(testCase.Name, func(t *testing.T) {
			root := t.TempDir()
			err := ProcessFile(testCase.Input, root, nameTemplate)

			if err != nil {
				t.Errorf("unexpected error processing manifest %s: %s", testCase.Input, err)
			}

			for _, outputManifest := range testCase.Creates {
				savedFile, err := ReaderFor(path.Join(root, outputManifest))
				if err != nil {
					t.Fatalf("unexpected error opening output %s: %s", outputManifest, err)
				}
				object := new(KubernetesObject)
				if err := yaml.NewDecoder(savedFile).Decode(object); err != nil {
					t.Fatalf("unexpected error decoding saved manifest: %s", err)
				}
				if object.Metadata.Name != "test" {
					t.Errorf("expected metadata.name to be test, got %s", object.Metadata.Name)
				}
			}
		})
	}
}

func TestReaderFor(t *testing.T) {

	testServer := httptest.NewServer(http.FileServer(http.Dir("testdata")))
	defer testServer.Close()

	testCases := []struct {
		Name     string
		Location string
		Error    bool
	}{
		{Name: "file", Location: "testdata/single.yaml"},
		{Name: "http", Location: fmt.Sprintf("%s/single.yaml", testServer.URL)},
		{Name: "file-fail", Location: "testdata/nonexistent.yaml", Error: true},
		{Name: "http-fail", Location: fmt.Sprintf("%s/nontexistent.yaml", testServer.URL), Error: true},
	}

	for _, testCase := range testCases {
		t.Run(testCase.Name, func(t *testing.T) {
			_, err := ReaderFor(testCase.Location)

			if testCase.Error && err == nil {
				t.Fatal("expected an error but none received")
			}
			if !testCase.Error && err != nil {
				t.Fatalf("unexpected error: %s", err)
			}
		})
	}

}
