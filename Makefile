.PHONY: build checksum clean coverage dep fmt image test vet

OS ?= $(shell go env GOOS)
ARCH ?= $(shell go env GOARCH)
IMAGE_NAME ?=  toolbag
IMAGE_TAG ?= latest

build: dep $(patsubst cmd/%,bin/$(OS)-$(ARCH)/%,$(wildcard cmd/*))

checksum:
	rm -f bin/SHA256SUM
	find bin/ -mindepth 2 -type f -exec sha256sum {} \; | sed 's@bin/@@' >> bin/SHA256SUM

clean:
	@go clean
	@rm -rf bin

coverage:
	@go test -cover -coverprofile=coverage.out ./...

image:
	docker build --rm --build-arg OS=$(OS) --build-arg ARCH=$(ARCH) --tag $(IMAGE_NAME):$(IMAGE_TAG) .

fmt:
	@go fmt ./...

test:
	@go test -cover ./...

vet:
	@go vet ./...

dep:
	@go mod download

bin/$(OS)-$(ARCH)/%: cmd/%  cmd/%/*
	@mkdir -p bin/$(OS)-$(ARCH)
	CGO_ENABLED=0 go build -ldflags="-s -w" -o $(@) $(shell go list ./$< )
