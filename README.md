# toolbag

A collection of (typically single-purpose) tools for dealing with DevOps tasks, and more.

## Requirements

* go 1.16+
* GNU make
* docker or podman

