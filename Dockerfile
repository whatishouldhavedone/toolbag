FROM docker.io/library/golang:1.19 as builder

WORKDIR /go/toolbag
COPY . .
ARG OS=linux
ARG ARCH=amd64
RUN make build

FROM scratch

WORKDIR /usr/bin
ARG OS=linux
ARG ARCH=amd64
COPY --from=builder /go/toolbag/bin/${OS}-${ARCH}/ /usr/bin/
USER 1000